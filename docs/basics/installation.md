---
sidebarDepth: 0
---

# Installation

Installing Jumplinks is, like any other ProcessWire module, a breeze.

## Quick Installation

1. Under *Modules*, go to the *New* tab.
2. Enter the class name (*ProcessJumplinks*) and click *Download & Install*.

## Manual Installation

1. Download the module from [**here**](https://github.com/rockettpw/jumplinks/archive/master.zip)
2. Copy the *ProcessJumplinks* folder to `site/modules`
3. Go to *Modules* in your admin panel, and find *ProcessJumplinks*. If it's not listed, you'll need to click *Refresh*.
4. Install the module and configure it, if needed.
