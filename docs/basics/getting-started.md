# Getting Started

Using Jumplinks is easy. We'll get started by working with some basic redirects.

## Creating a new Jumplink

Under *Setup*, navigate to *Jumplinks*. Click on *Register First Jumplink*. This will open the Entity Editor where you can provide the details about your jumplink.

The Entity Editor contains the following fields:

- Source
- Destination (fields for manual specification and page selection)
- Timed Activation

### Source

Enter the source route that you would like matched in the *Source* field.

> **A note about leading slashes:** As the module **always** works with routes relative to the root of your ProcessWire installation, you should always omit the leading slash (`/`) from all fields in this editor. If you include a leading slash, the module will trim it out before saving.

Here are some typical examples:

- about.php
- somepage.html
- category/somepage.html
- Services/default.aspx
- products.php?product_id=4

> **A note about index.php:** Jumplinks will *always* trim out any reference made to `index.php`, but only if it is suffixed with a path. To clarify, requests made to `/index.php` will remain as-is, but requests to `/index.php/something` will always be redirected to `/something`. If you want to create a jumplink for `/index.php/something`, then simply omit the `/index.php/` from the source URI.

### Destination

Now, you'll need to specify a destination for this route by either manually specifying it, or selecting it using the Page Tree or Auto Complete fields.

In most cases, you'll want to use the Page Tree or Auto Complete fields, unless you're making use of [wildcards](Wildcards), [mapping collections](Mapping-Collections), or absolute URLs to external sites. As such, using those fields is optional.

Naturally, the module allows you to make use of absolute external URLs. If you wish to use them, be sure to enter the full URL scheme (i.e. `http://` or `ftp://`).

Here are some examples, based on the previous Source examples:

- about/
- some-page/
- category/some-page/
- services/
- products/smartphones/

If you use the Page Tree or Auto Complete fields, the destination will be set to the page number of the selected page (in the format `page:<id>`) which ensures the destination always remains the same, even if the page's `name` changes.

### Timed Activation

The module allows you to make your jumplinks temporary by provisioning two picker-fields that allow you to set the starting and ending date and time. This feature is known as *Timed Activation*.

You can make use of *either/or* activation field. If only the start time is specified, the jumplink will only activate at such time. If only the end time is specified, the jumplink will activate from the moment it exists (upon first save) and will de-activate only at the time specified, unless it is changed.

## Editing and Deleting Jumplinks

You can edit your jumplinks by clicking on their respective source paths under *Setup* > *Jumplinks*. Doing so will open the the Entity Editor where your changes may be made.

### Info

In this mode, the Entity Editor also provides information about the jumplink being edited. Timestamps (when the jumplink was created, and when last it was updated), authors (the users responsible for creating and updating the jumplink) and last hit information is shown in the Info box.

### Clearing hits and deleting

Additionally, two checkbox fields are provided. The first is used to clear the hit counter for the jumplink being edited, and the second is used to delete it. If you'd like to perform any of these functions, simply check the respective checkbox, and click on *Update Jumplink*.

## About Stale Jumplinks

Since 1.3.0, the date and time of the last hit on a jumplink is recorded. The date can be seen by hovering over its hit counter (shown in light blue after the first hit), or by opening the Entity Editor and viewing the previously-described Info box.

If a jumplink hasn't been hit in the last 30 days, it will be highlighted in the main table, and a message stating that it is safe to delete will appear when editing it.

::: tip NOTE
Presently, this is hard-coded to 30 days, which is presumed to be an adequate amount of time before it is safe to delete an unused jumplink. However, if there are use-cases for different periods, please bring it up in the [forum](https://processwire.com/talk/topic/8697-jumplinks/).
:::
