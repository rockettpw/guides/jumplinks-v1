# Mapping Collections

## What on earth are mapping collections?

Good question. Essentially, they're lists you maintain of key/value pairs that you use in wildcards. Mapping collections come in handy when you're trying to redirect `category.php?catid=id-of-category` to `category/name-of-category/` using a single jumplink. Neat, right?

## How do I create one?

Under *Setup* > *Jumplinks*, switch to the "Mapping Collections" tab. Then click on "Install new Mapping Collection".

First, enter a name for the collection - this should be letters only, in lower-case. (In this running example, we'll assume the name is `categories`.)

As explained on the form that appears, mappings are defined in pairs, one on each line, in the following format: `key=value`. For example: `4=ramblings`.

Enter each mapping in the text-area provided, in the format above. Here's a quick example:

```
1=general
2=food
3=technology
4=ramblings
5=work
```

This doesn't only work with IDs. You can also use normal strings. Here's an unrelated example  for `{page}.html` : `{page|pages}/`:

```
privacy_policy=legal/privacy
terms_of_use=legal/terms
```

::: tip Slave Labour
Of course, it would take *forever* to type all these in if you have a large list of categories, so see if your old platform can export them in this format. If not, then write a small script that pulls them from your database and saves them in the format required.
:::

## Now what? How do I use them?

Basically, define a jumplink like this:

Source | Destination
------ | -----------
category.php?catid=`{id}` | category/`{id|categories}`/

How does this work? Simple: In this example, the source is looking for an ID. If one is matched according to the current request, it will look for that ID in the `categories` mapping collection and, if one is found, return the value of that mapping.

The format for the destination wildcard is `{wildcard-name|collection-name}`, where `wildcard-name` is the wildcard's identifier and `collection-name` is the name of the collection you've installed.

Now, if a request like `category.php?catid=4` is made, and that ID has been defined in the `categories` collection as `ramblings`, as above, Jumplinks will redirect that request to `category/ramblings/`

::: tip Housekeeping
Each mapping will be cleaned according to the [Wildcard Cleaning](/more/configuration.md#wildcard-cleaning) setting defined in the module's configuration.
:::

## What if the requested key hasn't been defined?

If the key does not exist in your collection, then it will simply replace the destination parameter with the key itself.

In our running example, `category.php?catid=124` would redirect to `category/124`, because `124` was not defined in the collection.
