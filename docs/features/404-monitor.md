# 404 Monitor

:::warning Note
This monitor is very basic in its functionality. There are plans to rewrite completely and move it to a separate module that integrates with Jumplinks.  The new module would have more features, too.
:::

Jumplinks ships with a 404 Monitor, which helps you keep track of hits to pages that do not exist so that you may later create jumplinks for them.

## Turning it on

By default, this feature is turned **off**.

To enable the monitor, go to the module's configuration page and, under 'Enable 404 Monitor', check 'Log 404 hits to the database'.

## Using it

Once enabled, a new tab will appear in the Jumplinks setup page called '404 Monitor'. The last 100 hits will be displayed in this table, with information concerning the request URI, referrer (if applicable), user agent, and the date/time each one was hit.

If you need to create a jumplink based on a 404-hit, simply click on its request URI. This will take you to the jumplink editor with the Source field pre-filled for you.

## Turning it off

If you no longer wish to use the monitor, go to the module's configuration page and uncheck the applicable checkbox described above.

:::tip Note
Turning the monitor off **will not** delete the records from the database. If you wish to remove them, you must turn the feature on, use the 'Clear All' button in the '404 Monitor' tab, and then turn it off again.
:::
