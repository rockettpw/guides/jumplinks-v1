# Destination Selectors

## An alternative to using Mapping Collections

Thanks to the [suggestion](https://processwire.com/talk/topic/8697-jumplinks/?p=93073) raised by [Macrura](https://processwire.com/talk/user/136-macrura/), you can now use selectors to find a destination page by using a field that contains a wildcard in the source path.

**Consider the following example:**

1. Let's say you want a page called "Our Solutions", located at `/our-solutions/`.
2. This page was previously located at `page.php?name=services`, for example.
3. This page will be assigned to the `basic-page` template.
4. You'd prefer to keep the information about where the page's previous location on the page itself, in a custom field.

Destination Selectors allow you to do this.

## How do I use them?

Continuing from our example above, create a field called `legacy_name` and assign it to the `basic-page` template. Then, create the page that you are migrating and call it "Our Solutions", per our running example.

Keeping in mind where the page was located before (`page.php?name=services`), we know that the only variable there is `services`. Enter that name into the `legacy_name` field.

Now, create a new jumplink with the following data:

Source | Destination
------ | -----------
page.php?name=`{name}` | \[\[template=basic-page,legacy_name=`{name}`\]\]

See those double-square-brackets? Those signify that a selector is being used. This selector tells Jumplinks to look for a page that is assigned to the `basic-page` template and contains the `legacy_name` being requested (`services`).

If the page exists, the selector (including the brackets) will be replaced with the page's URL (`$page->url`).

Thusly, a request to `page.php?name=services` will be redirected to `our-solutions/`.

If the page does not exist, the jumplink will be skipped immediately.

::: tip State of this feature
Right now, this feature is quite flexible, in that you can use multiple selectors and prepend/append those selectors with anything you like.

With that said, it is recommended that you only use one selector and it isn't recommended that you put anything *before* your selector, as that will result in a redirect to a page that doesn't exist.

Whilst it is *nice* to have the flexibility, there are not many use-cases for it. Nonetheless, the feature will continue to work in this manner for now.
:::
