# Wildcards

Unlike similar modules for other platforms, wildcards in Jumplinks are much easier to work with, as Regular Expressions are not fully exposed, and we don't use asterisks to mark wildcards. Instead, parameters wrapped in `{curly braces}` are used. Each wildcard is named and typed for easy identification. This makes your jumplinks cleaner, and much easier to work with.

As an example, instead of having this:

Source | Destination
------ | -----------
blog/`([0-9]+)`/`([0-9]+)`/`([a-z_-]+)`/ | posts/`$3`/

You'd have this:

Source | Destination
------ | -----------
blog/`{year:num}`/`{month:num}`/`{title:segment}`/ | posts/`{title}`/

By naming these parameters, we can easily identiy each part of the route is going where, and, if applicable, which ones are being discarded.

> **Smart Tip:** To make identification even easier, you can also use [smart wildcards](/features/smart-wildcards.md) which enable you to omit the type altogether (like this: `{page}`).

## Format

Each wildcard should be formatted like this: `{name:type}`

You can use any `name` you like, so long as it is only comprised of lower-case letters.

The following types are available:

Type | Matches | Pattern
---- | ------- | -------
`all` | Anything and everything, until another wildcard or marker | `.*`
`alpha` | Alpha characters (letters only) | `[a-z]+`
`alphanum` | Alpha-numeric characters (letters and numbers) | `\w+`
`any` | Any common character: letters, numbers, periods (.), hyphens, underscores, percent-signs, equal signs, and whitespace | `[\w.-_%\=\s]+`
`ext` | A common file extension | (see below)
`num` | Numeric characters (numbers only) | `\d+`
`segment` | A common segment: letters, numbers, hyphens, and underscores | `[\w_-]+`
`segments` | A set of common segments (separated by slashes) | `[\w/_-]+`

### Extensions

The following extensions are checked when the `ext` type is used:

`aspx`, `asp`, `cfm`, `cgi`, `fcgi`, `dll`, `html`, `htm`, `shtml`, `shtm`, `jhtml`, `phtml`, `xhtm`, `xhtml`, `rbml`, `jspx`, `jsp`, `phps`, `php4`, `php`

As much as `php`, `html` and `aspx` are the most common, we've included a more extensive checklist for the sake of covering almost everything.

## Housekeeping

Note that wildcards are cleaned according to the wildcard cleaning setting in the module's [configuration](/more/configuration.md). If you don't want your wildcards to be cleaned, simply use an exclamation mark in the destination wildcards, like this: `search/?q={!query}`

## Capture Prevention

As the module makes use of expressions, there may be cases where you don’t want a parameter/wildcard to be captured for the Destination URI/URL.

Normally, when writing a regular expression with a non-capture part, you’d wrap it like so: `(?:expression)`. Because of the nature of the module, the syntax has changed to `<expression>`.

So, if you wanted to check a URI for a certain set of strings that need not be captured, it would be formatted like this: `<string|another-string|or-something-else>`. See the example below and on the [examples](/more/tldr-examples.md) page for a practical use-case.

## Optional characters

If you need to mark a character as optional, there are two methods. You can either wrap the character in square braces (`[/]`) or by appending it with a question mark (`/?`), as you would with any regular expression.

## Some basic examples

Here are some basic examples to get you started:

Source | Destination URI
------ | ---------------
about-us.html | about/
page/`{page:segment}`.html | `{page}`/
cms/`{page:segment}`.`{extension:ext}` | `{page}`/
data/`{path:segments}` | `{path}`/
products.aspx?PID=`{product:id}` | products/`{product}`/
`<admin|backend>/?` | go-away-hacker/
