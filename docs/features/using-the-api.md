# Using the API

Jumplinks makes two public methods available to the ProcessWire API, allowing you to create jumplinks and collections via, for example, another module that automatically imports your site from another platform.

## Creating Jumplinks

If such module needs to create jumplinks, it needs to make a call to the public `add()` method:

```php
$this->modules->ProcessJumplinks->add('{id}/{title}', 'posts/{title}/');
```

Timed activation is also supported:

```php
$this->modules->ProcessJumplinks->add(
    'easter',
    'promos/easter-specials/',
    '2016-03-26 00:00:00', // start time
    '2016-03-28 12:59:00' // end time
);
```

The method doesn't return anything, so please make sure that the data you provide is correct.

In terms of validation, leading slashed will be stripped.

## Creating Collections

You may also find that you need to add mapping collections via the API.

This can be done with the `collection` call:

```php
$this->modules->ProcessJumplinks->collection('blogposts', [
    1 => 'this-is-a-test-post',
    2 => 'helloThere',
]);
```

And you don't have to use IDs:

```php
$this->modules->ProcessJumplinks->collection('pagenames', [
    'services' => 'solutions',
    'contact-us => 'connect',
]);
```

::: tip Housekeeping
As with adding them via the admin panel, all values will be cleaned according to the *Wildcard Cleaning* settings in the module's config. If *Enhanced Wildcard Cleaning* is enabled, then `helloThere` will become `hello-there` in order to conform with the standard ProcessWire uses.
:::

### Adding to collections

If you're running the function over a loop (post by post, or page by page), calling `collection()` with the name of an existing collection will add to it automatically.

Thanks to [Dave Damage](https://processwire.com/talk/user/2950-dave-damage/) for bringing the idea up.
