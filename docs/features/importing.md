# Importing

## Comma-Separated Values (CSV)

To import your redirects from CSV, switch to the *Import* tab under *Setup* > *Jumplinks* and click on *Import from CSV*.

You'll be presented with a form that contains two fields:

* A checkbox to specify if the CSV data contains headings
* A drop-down to select the delimeter your CSV data uses
* A drop-down to select the enclosure character your CSV data uses
* A text area for your CSV data

First, tell the importer whether or not your data includes headings via the applicable checkbox. Then, set your delimiter and enclosure characters that the data uses.

Paste in your old redirects into the text area, where each one is on its own line. Your CSV data must use the enclosure character specified in the applicable drop-down.

:::warning Note about CSV Parsing
Before v1.5.48, the importer attempted to guess which delimiter you were using, and was flexible regarding enclosure characters in the same manner. Unfortunately, an issue came up with the parser that was being used (Thanks to [@owzim](https://processwire.com/talk/profile/1092-owzim/) on the forums), and so [League\Csv](https://github.com/thephpleague/csv) is now used for CSV processing.
:::

### Available Columns

The following columns are available:

* Source
* Destination
* Time Start
* Time End

The last two columns are optional. When used, they should contain any valid date/time string. However, if they are used in one line, then all other lines should have blank entries for these columns (see the second example below).

This is the blueprint for each line: `"source","destination"[,"time_start","time_end"]`

### Do the Import

Then, click on *Import Data*, and your new jumplinks will be registered.

### Conversion Notes
* Any encoded ampersands (`&amp;`) will be converted to `&`.
* If the source or destination of a jumplink contains leading slashes, they will be stripped.
* Empty lines will be discarded

## ProcessRedirects

To import your redirects from [ProcessRedirects](https://modules.processwire.com/modules/process-redirects/), switch to the *Import* tab under *Setup* > *Jumplinks* and click on *Import from Redirects module*.

::: warning Note
This option won't be available if you don't have ProcessRedirects installed.
:::

You'll be presented with a table that contains all the redirects you created with ProcessRedirects.

If you want to skip any redirects, uncheck the box in the first column.

Then, click on *Import Data*, and your new jumplinks will be registered.

:::tip
The hit counter for each redirect will also be imported.
:::
