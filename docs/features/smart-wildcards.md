# Smart Wildcards

Simply put, smart wildcards are shorter wildcards with preset names that make assumptions about their type.

In other words, instead of declaring a wildcard with a `name` and a `type`, you'd just declare it with the `name`. For example, `{title:segment}` would become `{title}`.

The following smart wildcards are available:

Preset Name | Assumes Type
----------- | ------------
`all` | `all`
`ext` | `ext`
`name`, `title`, `page`, `post`, `user`, `model`,  `entry` and `segment` | `segment`
`path` and `segments` | `segments`
`year`, `month`, `day`, `id`, and `num` | `num`

### Examples

Source | Converts to (before processing)
------ | -----------------------------
page/`{page}`.html | page/`{page:segment}`.html
cms/`{page}`.`{ext}` | cms/`{page:segment}`.`{ext:ext}`
page.php?path=`{path}` | page.php?path=`{path:segments}`
about/`{segment}`/ | about/`{segment:segment}`/
app.php/`{segments}` | app.php/`{segments:segments}`
products.aspx?ProdName=`{name}` | products.aspx?ProdName=`{name:segment}`
