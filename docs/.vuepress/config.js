module.exports = {
  title: 'Jumplinks v1 Guide',
  description: 'Jumplinks, a ProcessWire module by Mike Rockett',
  port: 1234,
  evergreen: true,
  themeConfig: {
    repo: 'rockettpw/jumplinks',
    nav: [
      { text: 'v2 Tracker', link: 'https://github.com/rockettpw/jumplinks/projects/2' },
      { text: 'Issues', link: 'http://github.com/rockettpw/jumplinks/issues' },
      { text: 'Support', link: 'https://processwire.com/talk/topic/8697-jumplinks/' },
    ],
    sidebar: [
      {
        title: 'Prologue',
        collapsable: true,
        children: [
          '/',
          '/license'
        ]
      },
      {
        title: 'Basics',
        collapsable: true,
        children: [
          '/basics/installation',
          '/basics/getting-started'
        ]
      },
      {
        title: 'Features',
        collapsable: true,
        children: [
          '/features/wildcards',
          '/features/smart-wildcards',
          '/features/mapping-collections',
          '/features/destination-selectors',
          '/features/importing',
          '/features/404-monitor',
          '/features/using-the-api',
        ]
      },
      {
        title: 'More',
        collapsable: true,
        children: [
          '/more/configuration',
          '/more/tldr-examples',
        ]
      },
    ],
  }
}
