# TL;DR Examples

For reference, here are some examples of the different use-cases:

| Description | Source | Destination |
|---------------------------|-------------------------------|----------------------------------------------|
| Basic | About Us.html | about-us/ |
| [Wildcards](/features/wildcards.md) | `{page:segment}`.html | `{page}`/ |
| [Smart Wildcards](/features/smart-wildcards.md) | `{page}`.html<br>`{page}`.`{ext}` | `{page}`/ |
| [Mapping Collections](/features/mapping-collections.md) | product.php?id=`{id}` | products/`{id|products}` |
| [Destination Selectors](/features/destination-selectors.md) | blog/`{year}`/`{month}`/`{oldslug:segment}` | `[[template=blog_single, old_slug={oldslug}]]` |
| Optional trailing slashes | content/`{path}[/]`<br>**or:** content/`{path}/?` | `{path}`/ |
| Page IDs | index.html | page:1 |
| URLs | fb | `https://www.facebook.com/MyPage` |
| (no wildcard cleaning) | searc.php?q=`{!page}` | search/`{page}`/ |
| Capture Prevention | `wp-<admin|content|includes|login>{all}` | hacker-destination/ |
