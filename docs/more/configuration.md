# Configuration

Jumplinks comes with several configuration options:

* Wildcard Cleaning (and enhanced wildcard cleaning)
* Legacy Domain
* Debug Mode

## Wildcard Cleaning

Because Jumplinks uses wildcards, you may find that they need to be cleaned in order to be compatible with ProcessWire's naming standards. You're given three different cleaning options:

* **Full Clean** (recommended) - this is the default setting (i.e. you probably won't need to change it). Simply put, all wildcards will be converted to lower-case, and word-separators will be converted to hyphens. For example, `About%20Us` would be converted to `about-us`.
* **Clean, but don't change case** - Same as above, but the case won't be changed.
* **Don't clean at all** (not recommended) - This option is only there for the few developers who need paths to retain their integrity. 99% of folks won't use this.

::: tip Note
If you want a specific wildcard to not be cleaned, add an exclamation mark before its name in the destination URI/URL. For example: `search/?q={!query}`.
:::

## Enhanced Wildcard Cleaning

As the setting defines it: When enabled, wildcard cleaning goes a step further by means of breaking and hyphenating TitleCase wildcards, as well as those that contain abbreviations (ex: `NASALaunch`). Examples: `EnvironmentStudy` would become `environment-study` and `NASALaunch` would become `nasa-launch`.

Even though this is disabled by default, it's actually recommended that you turn this on. However, there may still be a few quirks here and there - please post on the [support thread](https://processwire.com/talk/topic/8697-module-jumplinks/) if you spot any.

## Legacy Domain

If you're migrating over to a new PW site slowly on a production server, and you have a legacy domain to retain old pages that you haven't created yet, then this feature is just for you.

Say you move your old site from `bigfirm.com` to `old.bigfirm.com`, and your new PW site is now running from the main domain, requests for the old site will still come into that domain. When specifying a Legacy Domain, Jumplinks will check to see if the requested page exists on the legacy domain. If it returns one of the accepted status codes (also defined in the configuration page), the user will be redirected to that domain.

::: tip Note
This check is performed *after* the check for redirects. So, if you've created a new page that would replace the old, and created a jumplink for it, then the legacy domain won't be checked.
:::

## Debug Mode

As defined in the module: If you run into any problems with your jumplinks, you can turn on debug mode. Once turned on, you'll be shown a scan log when a 404 is hit. That will give you an indication of what may be going wrong. If it doesn't, and you can't figure it out, then paste your log into the [support thread](https://processwire.com/talk/topic/8697-module-jumplinks/) on the forums.
