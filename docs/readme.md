---
title: Welcome
---

## Introduction

Jumplinks is an enhanced version of the original [ProcessRedirects](http://modules.processwire.com/modules/process-redirects/) by [Antti Peisa](https://twitter.com/apeisa).

The `Process` module manages your permanent and temporary redirects (we'll call these "jumplinks" from now on, unless in reference to redirects from another module), useful for when you're migrating over to ProcessWire from another system/platform.

Each jumplink supports wildcards, shortening the time needed to create them.

Unlike similar modules for other platforms, wildcards in Jumplinks are much easier to work with, as Regular Expressions are not fully exposed. Instead, parameters wrapped in curly braces are used - these are [described](/features/wildcards.md) in this documentation.

## Current Release & Requirements

**Current Release:** 1.5.49

As of version 1.5, Jumplinks requires at least ProcessWire 2.6.1 to run.

## Jumplinks 2

In the works is Jumplinks 2, which is a complete rewrite of the module that makes use of [FastRoute](https://github.com/nikic/FastRoute), Composer and Eloquent. The purpose of the rewite is to make the module future-proof and provide new features, enhancements, and increase the overall speed of the module (still to be benchmarked).

You can track development on the [project board](https://github.com/rockettpw/jumplinks/projects/2).

## Features

The most prominent features include:

- Basic jumplinks (from one fixed route to another)
- Parameter-based wildcards with "Smart" equivalents
- Mapping Collections (for converting ID-based routes to their named-equivalents without the need to create multiple jumplinks)
- Destination Selectors (for finding and redirecting to pages containing legacy location information)
- Timed Activation (activate and/or deactivate jumplinks at specific times)
- 404-Monitor (for creating jumplinks based on 404 hits)

Additionally, the following features may come in handy:

- Stale jumplink management
- Legacy domain support for slow migrations
- An importer (from CSV or ProcessRedirects)

## GitHub

Jumplinks is available on GitHub at http://github.com/rockettpw/jumplinks.

There is currently only one branch: [`master`](http://github.com/rockettpw/jumplinks), which contains the stable version of Jumplinks. The old `dev` branch has been removed completely, and a new branch will be created when Jumplinks 2 reaches alpha-ready status.


### Issues

If you stumble upon a bug, or any other like-minded creature, please [submit an issue](http://github.com/rockettpw/jumplinks/issues) to the issue tracker on Github.

## Requesting Support


At present, support may only be obtained by visiting the official [**forum thread**](https://processwire.com/talk/topic/8697-jumplinks/) on the ProcessWire Support Forums.

However, if your support request involves presenting information that may be deemed confidential, please [send me a private message on the forums](https://processwire.com/talk/profile/2289-mike-rockett/).

## Module Recommendations

Jumplinks complements your SEO-toolkit, which should also comprise of the following modules:

* [All In One Minify (AIOM+)](http://mods.pw/5q)
* [Page Path History (core)](http://mods.pw/2J)
* [XML Sitemap](http://mods.pw/1V)
* [Markup SEO](http://mods.pw/8D)
* [ProFields: AutoLinks](http://mods.pw/6d)
* [ProFields: ProCache](http://mods.pw/58)

## Open Source

Jumplinks is an open-source project, and is free to use. In fact, Jumplinks will always be open-source, and will always remain free to use. Forever. :tada:

::: tip Show your Support
If you would like to support the development of Jumplinks, please consider making a small [donation via PayPal](https://paypal.me/mrockettza/20).
:::

Learn more about:

* [Open Source Software](http://opensource.com/resources/what-open-source)
* [Free Software](https://en.wikipedia.org/wiki/Free_software)
